package com.lite.cloud.auth.springcloudnetflixauth;

import org.springframework.security.access.SecurityConfig;
import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;

public class Initializer extends AbstractHttpSessionApplicationInitializer {
    public Initializer() {
        super(SecurityConfig.class, Config.class);
    }
}
