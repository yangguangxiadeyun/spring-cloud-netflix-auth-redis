package com.lite.cloud.auth.springcloudnetflixauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudNetflixAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudNetflixAuthApplication.class, args);
	}
}
